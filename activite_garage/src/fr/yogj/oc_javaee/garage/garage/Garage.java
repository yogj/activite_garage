package fr.yogj.oc_javaee.garage.garage;

import java.io.Serializable;
import java.util.ArrayList;

import fr.yogj.oc_javaee.garage.vehicule.Vehicule;
/**
 * classe garage definit l'objet garage que l'on veut sauvegarder
 * attributs : liste de voiture
 * @author nicolas
 *
 */
public class Garage implements Serializable{

	private static final long serialVersionUID = 1L;
	private ArrayList<Vehicule> voiture; 


	/**
	 * constructeur par defaut 
	 */
	public Garage() {
		this.voiture = new ArrayList<Vehicule>();					
		}
	/**
	 * constructeur avec param
	 * @param pVoiture
	 */
	public Garage(ArrayList<Vehicule> pVoiture){
		this.voiture = pVoiture;
		}	

	
	/**
	 * m�thode toString qui decrit l'objet garage : decrit la liste des voitures
	 */
	public String toString() {
		String str = "";
		for (Vehicule v : voiture) {
			str = str+v.toString();
			}
		return str ;
		}
	/**
	 * methode addVoiture ajoute une voiture pVehicule au garage
	 * @param pVehicule
	 */
	public void addVoiture(Vehicule pVehicule) {
		this.voiture.add(pVehicule);
	}
}
