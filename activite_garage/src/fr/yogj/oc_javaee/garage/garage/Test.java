package fr.yogj.oc_javaee.garage.garage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fr.yogj.oc_javaee.garage.moteur.MoteurDiesel;
import fr.yogj.oc_javaee.garage.moteur.MoteurElectrique;
import fr.yogj.oc_javaee.garage.moteur.MoteurEssence;
import fr.yogj.oc_javaee.garage.moteur.MoteurHybride;
import fr.yogj.oc_javaee.garage.options.Barre_De_Toit;
import fr.yogj.oc_javaee.garage.options.Climatisation;
import fr.yogj.oc_javaee.garage.options.GPS;
import fr.yogj.oc_javaee.garage.options.Siege_Chauffant;
import fr.yogj.oc_javaee.garage.options.Vitre_Electrique;
import fr.yogj.oc_javaee.garage.vehicule.A300B;
import fr.yogj.oc_javaee.garage.vehicule.D4;
import fr.yogj.oc_javaee.garage.vehicule.Lagouna;
import fr.yogj.oc_javaee.garage.vehicule.Vehicule;

public class Test {

	public static void main(String[] args) {
		File fichier = new File("garage.txt");
		Garage garage;
		ObjectInputStream ois;
		ObjectOutputStream oos;
		
		try {
			if (fichier.exists()) { // si le fichier existe, on le lit
				try {
					
				ois = new ObjectInputStream(
						new BufferedInputStream(
								new FileInputStream(fichier)));
				
					System.out.println("********************************************************\n");
					System.out.println("*           GARAGE OPENCLASSROOMS                      *\n");
					System.out.println("********************************************************\n");
					System.out.println(((Garage)ois.readObject()).toString());
				ois.close();
				}catch (ClassNotFoundException e){
					e.printStackTrace();					
				}
			}
			
			else {	//creer le fichier et sauvegarder
				try {
					garage = new Garage();
										
					Vehicule lag1 = new Lagouna();
					lag1.setMoteur(new MoteurEssence("150 chevaux", 10256d));
					lag1.addOptions(new GPS());
					lag1.addOptions(new Siege_Chauffant());
					lag1.addOptions(new Vitre_Electrique());
					garage.addVoiture(lag1);
					
					Vehicule A300B_2 = new A300B();
					A300B_2.setMoteur(new MoteurElectrique("1500 W", 1234d));
					A300B_2.addOptions(new Climatisation());
					A300B_2.addOptions(new Barre_De_Toit());
					A300B_2.addOptions(new Siege_Chauffant());
					garage.addVoiture(A300B_2);
					
					Vehicule D4_1 = new D4();
					D4_1.setMoteur(new MoteurDiesel("200 Hdi", 25684.80d));
					D4_1.addOptions(new Barre_De_Toit());
					D4_1.addOptions(new Climatisation());
					D4_1.addOptions(new GPS());
					garage.addVoiture(D4_1);
					
					Vehicule lag2 = new Lagouna();
					lag2.setMoteur(new MoteurDiesel("500 Hdi", 456987d));
					garage.addVoiture(lag2);
					
					Vehicule A300B_1 = new A300B();
					A300B_1.setMoteur(new MoteurHybride("ESSENCE/Electrique", 12345.95d));
					A300B_1.addOptions(new Vitre_Electrique());
					A300B_1.addOptions(new Barre_De_Toit());
					garage.addVoiture(A300B_1);
					
					Vehicule D4_2 = new D4();
					D4_2.setMoteur(new MoteurElectrique("100 KW", 1224d));
					D4_2.addOptions(new Vitre_Electrique());
					D4_2.addOptions(new Barre_De_Toit());
					D4_2.addOptions(new Siege_Chauffant());
					D4_2.addOptions(new Climatisation());
					D4_2.addOptions(new GPS());
					garage.addVoiture(D4_2);
					
					oos = new ObjectOutputStream(
							new BufferedOutputStream(
									new FileOutputStream(fichier)));
					oos.writeObject(garage);
					oos.close();
					System.out.println(garage.toString()); // on affiche ce qui a ete ecrit dans le fichier pour controler
					
					ois = new ObjectInputStream(
							new BufferedInputStream(
									new FileInputStream(fichier)));
					
						System.out.println("********************************************************\n");
						System.out.println("*           GARAGE OPENCLASSROOMS                      *\n");
						System.out.println("********************************************************\n");
						System.out.println("Sauvegarde effectuee\n");
					ois.close();
				}catch (FileNotFoundException e) {
					e.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}

		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
}
}
}