package fr.yogj.oc_javaee.garage.moteur;

import java.io.Serializable;

/**
 * classe abstraite Moteur
 * attribut : type, cylindre, prix
 * @author nicolas
 *
 */
public abstract class Moteur implements Serializable{

	private static final long serialVersionUID = 1L;
	protected TypeMoteur type;
	protected String cylindre;
	protected double prix;
	
	/**
	 * methode abstraite setMoteur
	 * @param cylindre
	 * @param prix
	 */
	public abstract void setMoteur(String cylindre, double prix);
	/**
	 * methode setType definit le type de moteur construit
	 * @param pType
	 */
	public void setType(TypeMoteur pType) {
		this.type = pType;
	}
	/**
	 * methode getType renvoie le type de moteur
	 * @return
	 */
	public TypeMoteur getType() {
		return this.type;
	}
	/**
	 * methode getPrix renvoie le prix du moteur
	 * @return prix
	 */
	public double getPrix() {
		return this.prix;
	}
	/**
	 * methode getCylindre renvoie la cylindree
	 * @return cylindre
	 */
	public String getCylindre() {
		return this.cylindre;
	}

	/**
	 * methode toString renvoie la description de l'objet Moteur qui a ete cree
	 */
	public String toString() {
		String str = this.getType()+" "+this.getCylindre()+" ("+this.getPrix()+" �)" ;
		return str;
	}
}
