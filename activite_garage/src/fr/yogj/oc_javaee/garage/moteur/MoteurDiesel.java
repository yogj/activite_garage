package fr.yogj.oc_javaee.garage.moteur;

/**
 * classe MoteurDiesel fille de classe abstraite Moteur
 * attribut : cylindre, prix, motorisation
 * @author nicolas
 *
 */
public class MoteurDiesel extends Moteur {

	private static final long serialVersionUID = 1L;
	/**
	 * constructeur par defaut
	 */
	public MoteurDiesel() {
		this.cylindre = "";
		this.prix = 0;
		this.type = TypeMoteur.DIESEL;
	}
	/**
	 * constructeur avec param
	 * @param pCylindre
	 * @param pPrix
	 */
	public MoteurDiesel(String pCylindre, double pPrix) {
		this.cylindre = pCylindre;
		this.prix = pPrix;
		this.type = TypeMoteur.DIESEL;
	}
	/**
	 * methode setMoteur definit l'objet Moteur pour la classe MoteurDiesel
	 */
	public void setMoteur(String pCylindre, double pPrix) {
		this.cylindre = pCylindre;
		this.prix = pPrix;
		this.type = TypeMoteur.DIESEL;
	}
	
}
