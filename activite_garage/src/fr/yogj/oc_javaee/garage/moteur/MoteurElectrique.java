package fr.yogj.oc_javaee.garage.moteur;

/**
 * classe MoteurElectrique fille de classe abstraite Moteur
 * attributs : cylindre, prix, motorisation
 * @author nicolas
 *
 */
public class MoteurElectrique extends Moteur {

	private static final long serialVersionUID = 1L;
	/**
	 * constructeur par defaut
	 */
	public MoteurElectrique() {
		this.cylindre = "";
		this.prix = 0;
		this.type = TypeMoteur.ELECTRIQUE;
	}
	/**
	 * constructeur avec param
	 * @param pCylindre
	 * @param pPrix
	 */
	public MoteurElectrique(String pCylindre, double pPrix) {
		this.cylindre = pCylindre;
		this.prix = pPrix;
		this.type = TypeMoteur.ELECTRIQUE;
	}
	/**
	 * methode setMoteur definit l'objet Moteur pour les attributs de la classe MoteurElectrique
	 */
	public void setMoteur(String pCylindre, double pPrix) {
		this.cylindre = pCylindre;
		this.prix = pPrix;
		this.type = TypeMoteur.ELECTRIQUE;
	}
	
}
