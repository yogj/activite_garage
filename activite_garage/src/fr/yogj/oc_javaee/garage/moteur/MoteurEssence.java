package fr.yogj.oc_javaee.garage.moteur;

/**
 * classe MoteurEssence fille de classe abstraite Moteur
 * attributs : cylindre, prix, motorisation
 * @author nicolas
 *
 */
public class MoteurEssence extends Moteur {

	private static final long serialVersionUID = 1L;
	/**
	 * constructeur par defaut
	 */
	public MoteurEssence() {
		this.cylindre = "";
		this.prix = 0;
		this.type = TypeMoteur.ESSENCE;
	}
	/**
	 * constructeur avec param
	 * @param pCylindre
	 * @param pPrix
	 */
	public MoteurEssence(String pCylindre, double pPrix) {
		this.cylindre = pCylindre;
		this.prix = pPrix;
		this.type = TypeMoteur.ESSENCE;
	}
	/**
	 * methode setMoteur definit l'objet Moteur avec les attributs de la classe
	 */
	public void setMoteur(String pCylindre, double pPrix) {
		this.cylindre = pCylindre;
		this.prix = pPrix;
		this.type = TypeMoteur.ESSENCE;
	}
	
	
}
