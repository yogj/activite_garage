package fr.yogj.oc_javaee.garage.moteur;

/**
 * classe MoteurHybride fille de classe abstraite Moteur
 * attributs : cylindre, prix, motorisation
 * @author nicolas
 *
 */
public class MoteurHybride extends Moteur {

	private static final long serialVersionUID = 1L;
	/**
	 * constructeur par defaut
	 */
	public MoteurHybride() {
		this.cylindre = "";
		this.prix = 0;
		this.type = TypeMoteur.HYBRID;
	}
	/**
	 * constructeur avec param
	 * @param pCylindre
	 * @param pPrix
	 */
	public MoteurHybride(String pCylindre, double pPrix) {
		this.cylindre = pCylindre;
		this.prix = pPrix;
		this.type = TypeMoteur.HYBRID;
	}
	/**
	 * methode setMoteur definit l'objet Moteur pour les attributs de la classe
	 */
	public void setMoteur(String pCylindre, double pPrix) {
		this.cylindre = pCylindre;
		this.prix = pPrix;
		this.type = TypeMoteur.HYBRID;
	}
	
}
