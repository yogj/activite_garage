package fr.yogj.oc_javaee.garage.moteur;

import java.io.Serializable;

/**
 * enumeration TypeMoteur
 * attribut : nom
 * @author nicolas
 *
 */
public enum TypeMoteur implements Serializable {
	ESSENCE("moteur essence"),
	DIESEL("moteur diesel"),
	HYBRID("moteur hybride"),
	ELECTRIQUE("moteur electrique");
	
	private String nom ="";
	/**
	 * constructeur
	 * @param pNom
	 */
	TypeMoteur(String pNom) {
		this.nom = pNom;
	}
	/**
	 * methode toString retourne le nom du TypeMoteur
	 */
	public String toString() {
		return this.nom;
	}
}
