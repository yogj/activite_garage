package fr.yogj.oc_javaee.garage.options;

import java.io.Serializable;

/**
 * Classe barre de toit fille de interface Options
 * attributs : prix et nom
 * @author nicolas
 *
 */
public class Barre_De_Toit implements Options, Serializable {

	private static final long serialVersionUID = 1L;
	protected double prix = 0.00d;
	protected String nom = "Barres de toit";
	
	/**
	 * constructeur par defaut 
	 */
	public Barre_De_Toit() {
		this.prix = 29.90d;
		this.nom = "Barres de toit";
		}
	/**
	 * methode setPrix definit le prix de l'option barre de toit 
	 * @param pPrix
	 */
	public void setPrix(double pPrix) {
		this.prix = pPrix;
	}
	/**
	 * methode getPrix renvoie le prix de l'option barre de toit
	 * @return prix
	 */
	public double getPrix() {
		return this.prix;
	}
	/**
	 * methode getNom renvoie le nom de l'option barre de toit
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}

}
