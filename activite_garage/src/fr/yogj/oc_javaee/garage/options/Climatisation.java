package fr.yogj.oc_javaee.garage.options;

import java.io.Serializable;

/**
 * class climatisation fille de interface Options
 * attributs : prix et nom
 * @author nicolas
 *
 */
public class Climatisation implements Options, Serializable {

	private static final long serialVersionUID = 1L;
	protected double prix = 0.00d;
	protected String nom = "Climatisation";
	
	/**
	 * constructeur par defaut
	 */
	public Climatisation() {
		this.prix = 347.3d;
		this.nom = "Climatisation";
		}
	/**
	 * methode setPrix definit le prix de l'option climatisation
	 * @param pPrix
	 */
	public void setPrix(double pPrix) {
		this.prix = pPrix;
	}
	/**
	 * methode getPrix renvoie le prix de l'option climatisation
	 * @return prix
	 */
	public double getPrix() {
		return this.prix;
	}
	/**
	 * methode getNom renvoie le nom de l'option
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}

}
