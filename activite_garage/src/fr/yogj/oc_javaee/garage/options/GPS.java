package fr.yogj.oc_javaee.garage.options;

import java.io.Serializable;

/**
 * class GPS fille de interface Options
 * attributs : prix, nom
 * @author nicolas
 *
 */
public class GPS implements Options, Serializable {

	private static final long serialVersionUID = 1L;
	protected double prix = 0.00d;
	protected String nom = "GPS";
	
	/**
	 * constructeur par defaut
	 */
	public GPS() {
		this.prix = 112.5d;
		this.nom = "GPS";
		}
	/**
	 * methode setPrix definit le prix de l'option GPS
	 * @param pPrix
	 */
	public void setPrix(double pPrix) {
		this.prix = pPrix;
	}
	/**
	 * methode getPrix renvoie le prix de l'option GPS
	 * @return prix
	 */
	public double getPrix() {
		return this.prix;
	}
	/**
	 * methode getNom renvoie le nom de l'option
	 */
	public String getNom() {
		return this.nom;
	}

}
