package fr.yogj.oc_javaee.garage.options;

import java.io.Serializable;

/**
 * classe interface Options
 * @author nicolas
 *
 */
public interface Options extends Serializable{
	/**
	 * methode sans corps getPrix
	 * @return
	 */
		public double getPrix();
		/**
		 * methode sans corps getNom
		 * @return
		 */
		public String getNom();
		

}
