package fr.yogj.oc_javaee.garage.options;

import java.io.Serializable;

/**
 * classe Siege_Chauffant fille de interface Options
 * attributs : prix, nom
 * @author nicolas
 *
 */
public class Siege_Chauffant implements Options, Serializable {

	private static final long serialVersionUID = 1L;
	protected double prix = 0.00d;
	protected String nom = "Siege chauffant";
	
	/**
	 * constructeur par defaut
	 */
	public Siege_Chauffant() {
		this.prix = 562.9d;
		this.nom = "Siege chauffant";
		}
	/**
	 * methode setPrix determine le prix de l'option
	 * @param pPrix
	 */
	public void setPrix(double pPrix) {
		this.prix = pPrix;
	}
	/**
	 * methode getPrix retourne le prix de l'option
	 * @return prix
	 */
	public double getPrix() {
		return this.prix;
	}
	/**
	 * methode getNom retourne le nom de l'option
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}


}
