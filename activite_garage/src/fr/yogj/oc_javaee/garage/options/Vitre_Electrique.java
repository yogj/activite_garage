package fr.yogj.oc_javaee.garage.options;

import java.io.Serializable;

/**
 * classe Vitre_Electrique fille de interface Options
 * attributs prix, nom
 * @author nicolas
 *
 */
public class Vitre_Electrique implements Options, Serializable {

	private static final long serialVersionUID = 1L;
	protected double prix = 258.35d;
	protected String nom = "Vitres electriques";
	
	/**
	 * constructeur par defaut
	 */
	public Vitre_Electrique() {
		this.prix = 258.35d;
		this.nom = "Vitres electriques";
		}
	/**
	 * methode setPrix determine le prix de l'option
	 * @param pPrix
	 */
	public void setPrix(double pPrix) {
		this.prix = pPrix;
	}
	/**
	 * methode getPrix retourne le prix de l'option
	 * @return prix
	 */
	public double getPrix() {
		return this.prix;
	}
	/**
	 * methode getNom renvoie le nom de l'option
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}

}
