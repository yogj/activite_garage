package fr.yogj.oc_javaee.garage.vehicule;

import java.util.ArrayList;

import fr.yogj.oc_javaee.garage.moteur.Moteur;
import fr.yogj.oc_javaee.garage.options.Options;


/**
 * Classe A300B classe fille de classe Vehicule
 * attributs : nom, prix, nomMarque, options?
 * @author nicolas
 * 
 */
public class A300B extends Vehicule {

	private static final long serialVersionUID = 1L;
	
	
	/**
	 * constructeur par d�faut, recup celui de classe Vehicule
	 */
	public A300B() {
		super();
		nom = "A300B";
		prix = 28457.0d;
		nomMarque = Marque.PIGEOT;
	}
	/**
	 * constructeur avec parametres
	 * @param pPrix
	 * @param pNom
	 * @param pOptions
	 * @param pNomMarque
	 * @param pMoteur
	 */
	public A300B(double pPrix, String pNom, ArrayList<Options> pOptions, Marque pNomMarque, Moteur pMoteur) {
		super (pPrix, pNom, pOptions, pNomMarque, pMoteur);
	}
	
}
			