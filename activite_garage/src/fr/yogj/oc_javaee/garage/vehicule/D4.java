package fr.yogj.oc_javaee.garage.vehicule;


import java.util.ArrayList;

import fr.yogj.oc_javaee.garage.moteur.Moteur;
import fr.yogj.oc_javaee.garage.options.Options;
/**
 * classe D4 fille de classe Vehicule
 * attributs : nom, prix, nomMarque, options
 * @author nicolas
 *
 */
public class D4 extends Vehicule {

	private static final long serialVersionUID = 1L;


	/**
	 * constructeur par defaut
	 */
	public D4() {
		super();
		nom = "D4";
		prix = 25147.0d;
		nomMarque = Marque.TROEN;
	}
	/**
	 * constructeur avec parametres
	 * @param pPrix
	 * @param pNom
	 * @param pOptions
	 * @param pNomMarque
	 * @param pMoteur
	 */
	public D4(double pPrix, String pNom, ArrayList<Options> pOptions, Marque pNomMarque, Moteur pMoteur) {
		super (pPrix,pNom, pOptions, pNomMarque, pMoteur);
	}
	
}
