package fr.yogj.oc_javaee.garage.vehicule;

import java.util.ArrayList;

import fr.yogj.oc_javaee.garage.moteur.Moteur;
import fr.yogj.oc_javaee.garage.options.Options;


/**
 * classe Lagouna fille de classe Vehicule
 * attribut nom, prix, nomMarque, options
 * @author nicolas
 *
 */
public class Lagouna extends Vehicule{

	private static final long serialVersionUID = 1L;

	
	/**
	 * constructeur par defaut recup a la classe Vehicule
	 */
	public Lagouna() {
		super();
		nom = "Lagouna";
		prix = 23012.0d;
		nomMarque = Marque.RENO;
	}
	/**
	 * constructeur avec param recup a la classe Vehicule
	 * @param pPrix
	 * @param pNom
	 * @param pOptions
	 * @param pNomMarque
	 * @param pMoteur
	 */
	public Lagouna(double pPrix, String pNom, ArrayList<Options> pOptions, Marque pNomMarque, Moteur pMoteur) {
		super (pPrix,pNom, pOptions, pNomMarque, pMoteur);
	}
	
}
