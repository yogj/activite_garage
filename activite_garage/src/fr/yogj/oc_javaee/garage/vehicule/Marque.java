package fr.yogj.oc_javaee.garage.vehicule;

import java.io.Serializable;

/**
 * enumeration Marque
 * attribut nom
 * @author nicolas
 *
 */
public enum Marque implements Serializable {
	RENO("voiture RENO"),
	PIGEOT("voiture PIGEOT"),
	TROEN("voiture TROEN");
	
	private String nom = "";
	/**
	 * constructeur de l'enum
	 * @param pNom
	 */
	Marque(String pNom){
		this.nom = pNom;
	}
	/**
	 * methode toString renvoie le nom de la marque
	 * @return nom
	 */
	public String toString() {
		return this.nom;
	}
}
