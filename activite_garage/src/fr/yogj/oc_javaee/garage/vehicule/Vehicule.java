package fr.yogj.oc_javaee.garage.vehicule;



import java.io.Serializable;
import java.util.ArrayList;

import fr.yogj.oc_javaee.garage.moteur.Moteur;
import fr.yogj.oc_javaee.garage.options.Options;



/**
 * classe Vehicule
 * @author nicolas
 *attributs : prix, liste d'options, nomMarque, nom et moteur
 */

public class Vehicule implements Serializable {

	private static final long serialVersionUID = 1L;
	protected double prix;
	protected ArrayList<Options> options; 
	protected Marque nomMarque;
	protected String nom;
	protected Moteur moteur;
	/**
	 * constructeur par defaut
	 */
	public Vehicule() {

		this.prix = 0.00d;
		this.options = new ArrayList<Options>();
		this.setMarque(nomMarque);
		this.setMoteur(moteur);
		this.nom = "";
	}
	/**
	 * constructeur avec param
	 * @param pPrix
	 * @param pNom
	 * @param pOptions
	 * @param pNomMarque
	 * @param pMoteur
	 */
	public Vehicule(double pPrix, String pNom, ArrayList<Options> pOptions, Marque pNomMarque, Moteur pMoteur) {
		this.prix = pPrix;
		this.nom = pNom;
		this.options = pOptions;
		this.nomMarque = pNomMarque;
		this.moteur = pMoteur;
	}
	/**
	 * methode setMoteur definit les carac du moteur du vehicule
	 * @param pMoteur
	 */
	public void setMoteur(Moteur pMoteur){
		this.moteur = pMoteur;
	}
	public Moteur getMoteur() {
		return this.moteur;
	}
	/**
	 * methode getPrix renvoie le prix du vehicule
	 * @return prix
	 */
	public double getPrix() {
		return this.prix;
	}
	/**
	 * methode setPrix definit le prix du vehicule
	 * @param pPrix
	 */
	public void setPrix(double pPrix) {
		this.prix = pPrix;
	}
	/**
	 * methode getOption renvoie les options du vehicule
	 * @return denomination de option et prix de option
	 */
	public String getOption() {
		String str = "";
		double prixOption = 0.00d;
		String denomination ="";
		for (Options o : options) {
			denomination = o.getNom();
			prixOption = o.getPrix();
			str = str +denomination+" ("+prixOption+" �) ";
			}
		return str;
		
	}
	/**
	 * methode addOptions ajoute une option au vehicule
	 * @param pOption
	 */
	public void addOptions(Options pOption) {
			this.options.add(pOption);
	}
	/**
	 * methode getMarque retourne la marque du vehicule
	 * @return nomMarque
	 */
	public Marque getMarque() {
		return this.nomMarque;
	}
	/**
	 * methode setMarque determine la marque du vehicule
	 * @param pMarque
	 */
	public void setMarque (Marque pMarque) {
		this.nomMarque = pMarque;
	}
	/**
	 * methode getNom retourne le nom du vehicule
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}
	/**
	 * methode setNom definit le nom du vehicule
	 * @param pNom
	 */
	public void setNom(String pNom) {
		this.nom = pNom;
	}
	/**
	 * methode getPrixTotal determine le prix total du vehicule (prix options + prix vehicule)
	 * @return prix total
	 */
	public double getPrixTotal() {
		double prixInt = 0d;
		for(Options o : options)
			prixInt = prixInt + o.getPrix();
		return (prixInt + this.getPrix()+this.moteur.getPrix());
	}
	/**
	 * methode toString decrit le vehicule 
	 */
	public String toString() {
		return this.getMarque()+" : "+this.getNom()+" "+this.moteur.toString()+
				" [ "+this.getOption()+" ] d'une valeur totale "+this.getPrixTotal()+" �.\n";
	}

}
